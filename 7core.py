####################
#core7
#intellectual property of Max Osterried
#Team 7
####################
#importing libraries
####################
import random

import numpy as np
import math
import csv
import matplotlib.pyplot as plt
####################
#setting/importing variables
year = 2020
co2Price = (25 + 25 * ((year-2020) / 3)) / 1000000
pShip = 0.5 / 30000
pTruck = 2.65 / 8300

class tradePartner:
    def __init__(self, name, mass, value):
        self.name = name
        self.mass = int(mass)
        self.value = int(value)
        try:
            self.price_perKg = self.value / self.mass
        except:
            self.price_perKg = 0
        self.price = weight * self.price_perKg

    #transport cost
    def assignDistances(self, rows):
        self.dShip = int(rows[1])
        self.cShip = self.dShip * pShip * weight
        self.dTruck = int(rows[2])
        self.cTruck = self.dTruck * pTruck * weight

        self.emissions = self.dShip * 25 + self.dTruck * 100
        self.cem_perKg = self.emissions * co2Price
        self.cEmissions = self.cem_perKg * weight

        #self.totalShipment = self.cShip + self.cTruck + self.cEmissions

    #risk analysis
    def assignRisks(self, rows):
        self.pRisk = float(rows[1])
        #self.totalRisk = self.pRisk ** 2 * self.price
        self.riskyE_perKg = (1 - self.pRisk) * self.price_perKg
        self.totalRiskyE = self.riskyE_perKg * weight
        #self.combined = self.totalShipment + self.price + self.totalRisk
        self.combined = self.cEmissions + self.price - self.totalRiskyE


    def year(self, j):
        self.yearlyRisk = self.pRisk * j**0.1/10
        self.yearlyCEmissions = self.emissions * ((25 + 25 * ((j) / 3)) / 1000000)
        self.yearlyPrice = self.price_perKg * self.pRisk * (j**3/50)
        self.yearlyCombinate = (self.yearlyRisk ** 2 + self.yearlyCEmissions + self.yearlyPrice) * weight
        return self.yearlyCombinate


####################
#set trade partners via Sophias script
def parseComtradeData():
    with open(f'datasets/comtrade_{A}_{crop}.csv', newline='') as file:
        reader = csv.reader(file, delimiter="\n", quotechar="|")
        array = []
        for countryLine in reader:
            array += [countryLine[0].split(",")]
        comtradeData = []
        for i in range(1, len(array)):
            if int(array[i][26]) != 0:
                comtradeData += [[array[i][9], array[i][26], array[i][32]]]
        return comtradeData


def parseDistances():
    with open(f'datasets/distancesto_{A}.csv', newline='') as file:
        reader = csv.reader(file, delimiter="\n", quotechar="|")
        array = []
        for countryLine in reader:
            array += [countryLine[0].split(";")]
        unorderedDistances = []
        for i in range(1, len(array)):
            unorderedDistances += [[array[i][1], array[i][2], array[i][3]]]
        return unorderedDistances


def parseRisks():
    with open(f'datasets/risk_all.csv', newline='') as file:
        reader = csv.reader(file, delimiter="\n", quotechar="|")
        array = []
        for countryLine in reader:
            array += [countryLine[0].split(",")]
        unorderedRisks = []
        for i in range(1, len(array)):
            unorderedRisks += [[array[i][0], array[i][1]]]
        return unorderedRisks


####################
#search best values
def cheapest():
    cheapest = 0
    for i in range(1, len(tradePartners)):
        #if tradePartners[i].perKg < tradePartners[cheapest].perKg:
        if tradePartners[i].price < tradePartners[cheapest].price:
            cheapest = i
    return tradePartners[cheapest]

def safest():
    safest = 0
    for i in range(1, len(tradePartners)):
        #if tradePartners[i].totalRisk < tradePartners[safest].totalRisk:
        if tradePartners[i].totalRiskyE > tradePartners[safest].totalRiskyE:
            safest = i
    return tradePartners[safest]

def cleanest():
    cleanest = 0
    for i in range(1, len(tradePartners)):
        #if tradePartners[i].emissions < tradePartners[cleanest].emissions:
        if tradePartners[i].cEmissions < tradePartners[cleanest].cEmissions:
            cleanest = i
    return tradePartners[cleanest]

def best():
    best = 0
    for i in range(1, len(tradePartners)):
        if tradePartners[i].combined < tradePartners[best].combined:
            best = i
    return tradePartners[best]


####################
#certify trade route
def certifyPrice(index):
    cheaper = 1
    for i in range(0, len(tradePartners)):
        #if tradePartners[i].perKg < tradePartners[index].perKg:
        if tradePartners[i].price< tradePartners[index].price:
            cheaper += 1
    return cheaper


def certifyRisk(index):
    safer = 1
    for i in range(0, len(tradePartners)):
        #if tradePartners[i].totalRisk < tradePartners[index].totalRisk:
        if tradePartners[i].totalRiskyE > tradePartners[index].totalRiskyE:
            safer += 1
    return safer


def certifyEnv(index):
    cleaner = 1
    for i in range(0, len(tradePartners)):
        #if tradePartners[i].emissions < tradePartners[index].emissions:
        if tradePartners[i].cEmissions < tradePartners[index].cEmissions:
            cleaner += 1
    return cleaner


####################
#MAIN

#input
A = "Germany"
crop = "soy"
weight = 5000

def importData():
    #read and order files
    comtradeArray = parseComtradeData()

    unorderedDistances = parseDistances()
    unorderedRisks = parseRisks()

    global tradePartners
    tradePartners = []
    for cell in comtradeArray:
        tradePartners.append(tradePartner(cell[0], cell[1], cell[2]))
    for partner in tradePartners:
        for rows in unorderedDistances:
            if rows[0] == partner.name:
                partner.assignDistances(rows)
        for rows in unorderedRisks:
            if rows[0] == partner.name:
                partner.assignRisks(rows)






def rankRoutes():
    importData()

    print(f"7CORE: Cheapest import: {cheapest().name} with {cheapest().price}$ costs.")
    print(f"7CORE: Safest route: {safest().name} with {safest().totalRiskyE}$ in risk.")
    print(f"7CORE: Best environmental impact: {cleanest().name} with {cleanest().cEmissions}$ CO2.")
    print(f"7CORE: Best overall score: {best().name} with {best().combined}$ combined cost.")


def certifyRoute(country):
    importData()

    for i in range(0, len(tradePartners)):
        if tradePartners[i].name == country:
            index = i
            break

    print(f"7CORE: Your route is place {certifyEnv(index)} in terms of environmental impact.")
    print(f"7CORE: Your route is place {certifyRisk(index)} in terms of risk analysis.")


#calculate the ideal mix of imports
def optimizeRoutes():
    importData()

    #define two matrices for key values and import masses per country
    raster = np.zeros([len(tradePartners), 3])
    importedMasses = np.zeros([1, len(tradePartners)])
    for i in range(0, len(tradePartners)):
        raster[i][0] = tradePartners[i].price_perKg
        raster[i][1] = tradePartners[i].cEmissions
        raster[i][2] = tradePartners[i].riskyE_perKg


    importedMasses[0][0] = weight
    combined = importedMasses @ raster
    bestDistribution = importedMasses
    bestValue = combined[0][0] + combined[0][1] + combined[0][2]
    for i in range(0, 10000):
        importedMasses = np.zeros([1, len(tradePartners)])
        currentMass = 0
        while currentMass+10 < weight:
            randomMass = random.randint(0, weight - currentMass)
            currentMass += randomMass
            importedMasses[0][random.randint(0, len(importedMasses))] += randomMass
        importedMasses[0][random.randint(0, len(importedMasses))] += weight -currentMass


        combined = importedMasses @ raster
        newValue = combined[0][0] + combined[0][1] + combined[0][2]
        if newValue < bestValue:
            bestValue = newValue
            bestDistribution = importedMasses

    print(bestValue)
    print(bestDistribution)


def plotRoutes():
    importData()

    raster = np.empty([len(tradePartners), 5])
    for i in range(0, len(tradePartners)):
        for j in range(0, 5):
            raster[i][j] = tradePartners[i].year(j)

    startIndex = 0
    averageIndex = 0
    endIndex = 0
    for i in range(1, len(tradePartners)):
        if raster[i][0] < raster[startIndex][0]:
            startIndex = i
        if raster[i][4] < raster[endIndex][4]:
            endIndex = i
        if np.sum(raster[i]) < np.sum(raster[averageIndex]):
            averageIndex = i
    plt.plot(raster[startIndex])
    plt.ylabel(f"Best now: {tradePartners[startIndex].name}")
    plt.show()
    plt.plot(raster[endIndex])
    plt.ylabel(f"Best future: {tradePartners[endIndex].name}")
    plt.show()
    plt.plot(raster[averageIndex])
    plt.ylabel(f"Best in range: {tradePartners[averageIndex].name}")
    plt.show()



run = True
print("--------------------------------------------------------------------------------")
print("--------------------------------------------------------------------------------")
print("Welcome to 7CORE. A global traderoutes identifier.")
print("--------------------------------------------------------------------------------")
print("A list of commands can be accessed with 'help'.")
print("--------------------------------------------------------------------------------")
while run:
    request = input("7CORE <<< ")
    if "help" in request:
        print("7CORE commands: ")
        print("     Calculate the best traderoutes: import [weight] of [crop]")
        print("     Certify your existing route: certify [crop] from _[country]")
        print("     Shut down the program: kill")
    elif "import" in request:
        try:
            weight = int(request.split()[1])
            crop = request.split()[3]
            rankRoutes()
        except:
            print("7CORE: Invalid input for 'import' keyword.")
    elif "certify" in request:
        try:
            weight = 1000
            crop = request.split()[1]
            country = request.split("_")[1]
            certifyRoute(country)
        except:
            print("7CORE: Invalid input for 'certify' keyword.")
    elif "predictImport" in request:
        #try:
            weight = int(request.split()[1])
            crop = request.split()[3]
            plotRoutes()
        #TODO: rank the routes for the next 30 years and plot the best starter, the best at the end and the best average
        #except:
            #print("7CORE: Invalid input for 'predictImport' keyword.")
    elif "futureProof" in request:
        try:
            weight = int(request.split()[1])
            crop = int(request.split()[3])
            country = int(request.split()[5])
            certifyRoute(country)
        #TODO: rank the certificate for the next 30 years and plot it
        except:
            print("7CORE: Invalid input for 'certify' keyword.")
    elif "optimize" in request:
        #try:
            weight = int(request.split()[1])
            crop = request.split()[3]
            optimizeRoutes()
    elif "kill" in request:
        print("7CORE shutting down.")
        run = False
    else:
        print("7CORE: Invalid command.")
    print("--------------------------------------------------------------------------------")
