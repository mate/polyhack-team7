require(spam64)
require(spam)
library(ggplot2)
library(readr)
library(forecast)
library(reshape2)
#library(fpp2)
#library(TTR)
library(dplyr)
library(fpp3)
comtrade2020 = read.csv('../comtrade.csv')
str(comtrade2020)
summary(comtrade2020)
comtrade2020_sub = comtrade2020[,c(7,10,32)]
ct2020_wide = reshape(data=comtrade2020_sub, idvar = 'Reporter', timevar = 'Trade.Flow.Code', v.names = 'Trade.Value..US..', direction = 'wide')

ct2020_wide[is.na(ct2020_wide)] = 0
ct2020_wide$sum = rowSums(ct2020_wide[,2:5], dims = 1)
row.names(ct2020_wide) = ct2020_wide[,1]
ct2020_wide = ct2020_wide[,-1]
colnames(ct2020_wide) = c('Import','Export','ReImport','ReExport','Sum')
str(ct2020_wide)
par(mfrow=c(2,2))
with(ct2020_wide, {
  hist(Import)
  hist(Export)
  hist(ReImport)
  hist(ReExport)
})
par(mfrow=c(1,1))
hist(ct2020_wide$Sum)
hc = hclust(dist(ct2020_wide$Sum),method = 'ward.D')
demd = as.dendrogram(hc)
plot(hc,labels=row.names(ct2020_wide), main = "Hierarchical Clustering")
rect.hclust(hc, k=3, border="red")
cutree(hc,k=3)

Cate1 = rownames(ct2020_wide[which(cutree(hc,k=3)==1),])
Cate2 = rownames(ct2020_wide[which(cutree(hc,k=3)==2),])
Cate3 = rownames(ct2020_wide[which(cutree(hc,k=3)==3),])

#Preprocessing data from last 20 years
G90_94 = read.csv('../comtrade94-90.csv')
G95_99 = read.csv('../comtrade95-99.csv')
G00_04 = read.csv('../comtrade2000-2004.csv')
G05_09 = read.csv('../comtrade2005-2009.csv')
G10_14 = read.csv('../comtrade2010-2014.csv')
G15_19 = read.csv('../comtrade2015-2019.csv')
G20 = read.csv('../comtrade2020.csv')
G90_94_sub = G90_94[,c(2,13,30,32)]
G95_99_sub = G95_99[,c(2,13,30,32)]
G00_04_sub = G00_04[,c(2,13,30,32)]
G05_09_sub = G05_09[,c(2,13,30,32)]
G10_14_sub = G10_14[,c(2,13,30,32)]
G15_19_sub = G15_19[,c(2,13,30,32)]
G20_sub = G20[,c(2,13,30,32)]
G90_20 = rbind(G90_94_sub,G95_99_sub,G00_04_sub,G05_09_sub,G10_14_sub,G15_19_sub,G20_sub)

G90_20$VperKg = (G90_20$Trade.Value..US..)/ (G90_20$Netweight..kg.)
G90_20_TradeValue = reshape(data=G90_20, idvar = 'Partner', timevar = 'Year', v.names = 'Trade.Value..US..', direction = 'wide')
G90_20_NetWeight = reshape(data=G90_20, idvar = 'Partner', timevar = 'Year', v.names = 'Netweight..kg.', direction = 'wide')
G90_20_Vperkg = reshape(data=G90_20, idvar = 'Partner', timevar = 'Year', v.names = 'VperKg', direction = 'wide')
G90_20_TradeValue = G90_20_TradeValue[,-2]
G90_20_NetWeight = G90_20_NetWeight[,-2]
G90_20_Vperkg = G90_20_Vperkg[,C(-2,-3)]
G90_20_TradeValue = G90_20_TradeValue[,sort(names(G90_20_TradeValue))]
G90_20_NetWeight = G90_20_NetWeight[,sort(names(G90_20_NetWeight))]
G90_20_Vperkg = G90_20_Vperkg[,sort(names(G90_20_Vperkg))]
#G90_20_sorted[is.na(G90_20_sorted)] = 0
#ct2020_wide$sum = rowSums(ct2020_wide[,2:5], dims = 1)
row.names(G90_20_TradeValue) = G90_20_TradeValue[,1]
G90_20_TradeValue = G90_20_TradeValue[,-1]
colnames(G90_20_TradeValue) = (1991:2020)

row.names(G90_20_NetWeight) = G90_20_NetWeight[,1]
G90_20_NetWeight = G90_20_NetWeight[,-1]
colnames(G90_20_NetWeight) = (1991:2020)

row.names(G90_20_Vperkg) = G90_20_Vperkg[,1]
G90_20_Vperkg = G90_20_Vperkg[,-1]
colnames(G90_20_Vperkg) = (1991:2020)
write.csv(G90_20_NetWeight,"Data/fullNetweight.csv")
write.csv(G90_20_TradeValue,"Data/fullTradevalue.csv")
write.csv(G90_20_Vperkg,"Data/fullVperkg.csv")


Country_names = data.frame(row.names(G90_20_sorted))
write.csv(Country_names,"../Countrynames.csv")
G90_20_TradeValue[is.na(G90_20_TradeValue)] = 0
#Time series forcasting
forcasting = c()
for (i in 1:87){
  y <- as.double(G90_20_sorted[i,])
  dat_ts <- as.ts(y, start = c(1991), end = c(2020),frequency = 1)
  model_tbats <- tbats(dat_ts)
  for_tbats <- forecast::forecast(model_tbats, h = 5)
  df_tbats = as.data.frame(for_tbats)
  forcasting[i] = df_tbats
}
G2025 = c()
for(i in 1:87){
  G2025 = rbind(G2025,forcasting[[i]][5])
  print(forcasting[[i]][5])
}
G2025 = data.frame(G2025)
G2025[which(G2025$G2025<500),] = 0
y <- as.double(G90_20_sorted[1,])
ts.plot(y)
dat_train = y[1:25]
dat_test = y[26:30]
m <- 5
mape <- function(actual,pred){
    mape <- mean(abs((actual - pred)/actual))*100
    return (mape)
}
dat_ts <- as.ts(dat_train, start = c(1991), end = c(2005),frequency = 1)
naive_mod <- naive(dat_ts, h = 5)
mape(dat_test,naiv)
arima_model <- auto.arima(dat_ts)
summary(arima_model)
fore_arima = forecast::forecast(arima_model, h=5)
df_arima = as.data.frame(fore_arima)
df_arima
model_tbats <- tbats(dat_ts)
summary(model_tbats)
for_tbats <- forecast::forecast(model_tbats, h = 5)
df_tbats = as.data.frame(for_tbats)


mm = melt(G90_20_sorted, id='id')
ggplot(mm,                            # Draw ggplot2 time series plot
       aes(x = variable,
           y = value,
           col = id)) +
  geom_line()
ggplot(mm)+geom_line(aes(x=variable, y=value, group=cate ,color=cate))

lapply(G2025, function(x){ length(which(x==0))/length(x)})
G2025_forcasting = cbind(Country_names,G2025)
G2025_forcasting[G2025_forcasting==0] <- NA
G2025_forcasting<-G2025_forcasting[complete.cases(G2025_forcasting),]
colnames(G2025_forcasting) = c('Country','Value')
write.csv(G2025_forcasting,"../G2025_forcasting.csv")

hc1 = hclust(dist(G2025_forcasting[2:56,]$Value),method = 'ward.D')
demd = as.dendrogram(hc1)
plot(hc1,labels=G2025_forcasting[2:56,1], main = "Hierarchical Clustering")
rect.hclust(hc1, k=3, border="red")
cutree(hc,k=3)
#plot

library(ggplot2)
library(scales)
library(gridExtra)

#example

Edata_v = G90_20_TradeValue[c(1,3,4,6),]
Edata_w = G90_20_NetWeight[c(1,3,4,6),]
Edata_p = G90_20_Vperkg[c(1,3,4,6),]
row.names(Edata_v) = Edata_v$Partner
Edata_v = Edata_v[,-1]
colnames(Edata_v) = c(1991:2020)
row.names(Edata_w) = Edata_w$Partner
Edata_w = Edata_w[,c(-1,-32)]
row.names(Edata_p) = Edata_p$Partner
Edata_p = Edata_p[,c(-1,-2)]
v1 = as.double(Edata_v[1,])
v2 = as.double(Edata_v[2,])
v3 = as.double(Edata_v[3,])
v4 = as.double(Edata_v[4,])
t = c(1991:2020)
plot(t,v1, col=2, xlab= 'Year', ylab='Values', type = 'l')
lines(t,v2 ,col = 1)
plot(t,v3 ,col = 1, xlab= 'Year', ylab='Values',type = 'l')
plot(t,v4 ,col = 1, xlab= 'Year', ylab='Values',type = 'l')
legend(2,1,c("decay","growth"), lwd=c(5,2), col=c("green","red"), y.intersp=1.5)
plot(x= c(1991:2020),y = v2 ,col = 1)

vv = melt(Edata_v, id.vars = 'Partner')
ggplot(vv,                            # Draw ggplot2 time series plot
       aes(x = variable,
           y = value,
           col = Partner)) +
  geom_line()

ggplot(vv)#+geom_line(aes(x=variable, y=value, color=Partner))

mm = melt(G90_20_sorted, id='id')
ggplot(mm,                            # Draw ggplot2 time series plot
       aes(x = variable,
           y = value,
           col = id)) +
  geom_line()
ggplot(mm)+geom_line(aes(x=variable, y=value, group=cate ,color=cate))
library(xts)
library(xtsExtra)
Edata_pw = t(Edata_p)
ts.plot(Edata_pw, gpars= list(col=1:4),xlab="year", main = 'Value per kg')
legend("topleft", legend = c('World','Austria','Brazil','China'), col = 1:4, lty = 1)

Edata_vw = t(Edata_v)
Edata_ww = t(Edata_w)
ts.plot(Edata_vw, gpars= list(col=1:4), xlab="year", main = 'Net weight')
legend("topleft", legend = c('World','Austria','Brazil','China'), col = 1:4, lty = 1)
ts.plot(Edata_ww, gpars= list(col=1:4),xlab="year", main = 'Trade Value')
legend("topleft", legend = c('World','Austria','Brazil','China'), col = 1:4, lty = 1)
G90_20_TradeValue[is.na(G90_20_TradeValue)] = 0
forcasting = c()
for (i in 1:4){
  y <- na.omit(as.double(Edata_p[i,]))
  dat_ts <- as.ts(y, start = c(1991), end = c(2020),frequency = 1)
  model_tbats <- tbats(dat_ts)
  for_tbats <- forecast::forecast(model_tbats, h = 5)
  df_tbats = as.data.frame(for_tbats)
  forcasting[i] = df_tbats
}

prediction = cbind(c(0.375,0.375,0.375,0.375),c(0.678,0.678,0.678,0.678),c(0.421,0.421,0.421,0.421),c(1.49,1.49,1.49,1.49))
 
Edata_pw = rbind(Edata_pw,prediction)
ts.plot(Edata_pw, gpars= list(col=1:4),xlab="year", main = 'Value per kg')
legend("topleft", legend = c('World','Austria','Brazil','China'), col = 1:4, lty = 1)
