# Polyhack-team7
We'll win. Here's how:

- Identify most important trade routes
- Analyze Correntics' set of goals

- Brainstorm for a system, that will help Correntics' customers to be more environmentally friendly and to also future-proof their trade routes.

- Implement the system for the identified trade routes

- give reasons why to adopt our system
    + risk reduction
    + public relations
    + safety in future legislation (CO2 price)

- build a presentation from that

- win



- We need Datasets:
  + countries.csv with countries indexed like in Sophias plot
  + risk.csv 
    + y: indexed as the countries in countries.csv 
    + x: risk in percent * damage for every mayor natural catastrophe
  + tradingPartner_[crop].csv for every crop we want to analyze. 
    + y: indexed as the countries in countries.csv 
    + x: the indexes of every trading partner as an integer
  + distance.csv 
    + y: indexed as the countries in countries.csv 
    + x: the distance to the main trading route in the country at index x
    + merge with the means of transportation later